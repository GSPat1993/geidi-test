import { Component, ViewChild } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Nav, Platform } from 'ionic-angular';

import { AppService } from '../app/app.service';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  // rootPage:any = LoginPage;
  pages: Array<{ title: string, component: any }>;
  subject: any;

  constructor(
    public platform: Platform,
    public appServ: AppService,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.pages = [
      { title: 'Page One', component: HomePage },
      { title: 'Page Two', component: LoginPage }
    ];
  }

  ngOnInit() {
    (localStorage.getItem('email')) ? this.nav.setRoot(HomePage) : this.nav.setRoot(LoginPage);
    this.appServ.watchLogin().subscribe(data => {
      console.log('DATA: ', data);
      (data) ? this.nav.setRoot(HomePage) : this.nav.setRoot(LoginPage);
    });
  }
}

