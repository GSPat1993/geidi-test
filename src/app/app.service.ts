import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import * as _ from 'lodash';

@Injectable()
export class AppService {
  loginLogger = new Subject();
  projectLogger = new Subject();

  constructor( ) {
    console.debug('Hello AppService Provider');
  }

  watchLogin(): Observable<any> {
    return this.loginLogger.asObservable()
  }

  watchProjectList(): Observable<any> {
    return this.projectLogger.asObservable()
  }

  setProjects(projects) {
    localStorage.setItem('projects', JSON.stringify(projects));
    this.projectLogger.next(_.values(JSON.parse(localStorage.getItem('projects'))));
  }

  setLogin(email) {
    return new Promise((resolve, reject) => {
      localStorage.setItem('email', email);
      this.loginLogger.next(localStorage.getItem('email'));
      resolve(localStorage.getItem('email'));
    });
  }

  resetLogin() {
    localStorage.removeItem('email');
    this.loginLogger.next(localStorage.getItem('email'));
  }
}