import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { AppService } from './app.service';

import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';

import { ControlMessagesComponent } from '../validators/control-message.component';

import { LoginService } from '../pages/login/login.service';
import { ValidationService } from '../validators/validation.service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    ControlMessagesComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage
  ],
  providers: [
    StatusBar,
    AppService,
    LoginService,
    SplashScreen,
    ValidationService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
