import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { LoadingController } from 'ionic-angular';

import { ValidationService } from '../../validators/validation.service';

import { AppService } from '../../app/app.service';
import { LoginService } from './login.service';

import { HomePage } from '../home/home';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {

  loader: any;
  login_form: any;

  constructor(
    public fb: FormBuilder,
    public appServ: AppService,
    public loadingCtrl: LoadingController,
  ) {

      this.login_form = this.initForm();
    }

  ionViewDidLoad() {
    console.debug('Hello LoginPage Page');
  }

  initForm() {
    return this.fb.group({
      email: ['', [Validators.required, ValidationService.emailValidator]],
      password: ['', [Validators.required, ,Validators.minLength(4)]]
    });
  }

  login(event) {
    this.loader = this.loadingCtrl.create({
      content: 'logging in...'
    });
    this.loader.present();
    this.appServ.setLogin(this.login_form.value.email).then((resolve) => {
      console.log(resolve);
      this.loader.dismiss();
    });
  }
}
