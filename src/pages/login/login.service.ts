import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';


@Injectable()
export class LoginService {

  constructor() {
    console.debug('Hello LoginService Provider');
  }

  saveAuth(email) {
    return new Promise((resolve, reject) => {
      localStorage.setItem('email', email);
      resolve(localStorage.getItem('email'));
    });
  }

}
