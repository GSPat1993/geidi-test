import { Component } from '@angular/core';
import { AlertController } from 'ionic-angular';

import { AppService } from '../../app/app.service'

import * as _ from 'lodash';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  project: any;
  projectList: any;

  constructor(
    public appServ: AppService,
    public alertCtrl: AlertController
  ) {

  }

  ngOnInit() {
    this.projectList = _.values(JSON.parse(localStorage.getItem('projects')));
    this.appServ.watchProjectList().subscribe(data => {
      this.projectList = data;
      console.log('DATA: ', data);
    });
  }

  addProject() {
    const projects: Array<any> = _.values(JSON.parse(localStorage.getItem('projects')));
    let duplicate: boolean;
    
    if (this.project) {
      if(!_.isEmpty(projects)) {
        duplicate = _.some(projects, (value) => {
          return (((value.name).toLowerCase().replace(/\s/g, '') === (this.project).toLowerCase().replace(/\s/g, '')) ? true : false);
        });

        (duplicate) ? this.showPrompt('DUPLICATE', 'Project name already taken.') : projects.push({name: this.project});
      } else {
        projects.push({name: this.project});
      }
      
      this.appServ.setProjects(projects);
    } else {
      this.showPrompt('FIELD EMPTY', 'Please enter something in the field.');
    }

    this.project = "";
  }

  showPrompt(title, message) {
    let alert = this.alertCtrl.create({
      title,
      subTitle: message,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  logout() {
    this.appServ.resetLogin();
  }

}
