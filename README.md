This is a Geidi Technical Exam for Senior Software Developer

## How get this repo

```bash
$ git clone https://GSPat1993@bitbucket.org/GSPat1993/geidi-test.git
$ cd geidi-test
```

## After entering the project folder, install all dependencies:

```bash
$ npm install
```
## To run the Project:

```bash
$ ionic serve
```

## To run the Karma Testing:

```bash
$ npm run test
```
## To run the E2E Testing:

```bash
$ npm run e2e
```